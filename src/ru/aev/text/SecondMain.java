package ru.aev.text;

import java.io.*;
import java.util.Scanner;

/**
 * @author Vorozheikin A.E
 *
 * Программа служит для подсчёта слов и символов в предложен(ие/ях).
 *
 * Пользователь вводит, где хранится его файл, далее:
 * 
 * Класс BufferedReader считывает текст из символьного потока ввода, буферизируя прочитанные символы.
 * Использование буфера призвано увеличить производительность чтения данных из потока.
 *
 * Считывая текст из файла построчно, проходимся по каждой строчке отдельно, узнавая:
 * кол-во слов, кол-во пробелов, кол-во символов. Передавая значения в переменные:
 * sumSpace(Сумма(кол-во) пробелов), sumSymbols(Сумма(кол-во) пробелов), sumWord(Сумма(кол-во) слов).
 *
 * Для каждой отдельной строчки с помощью метода printf() выводим результат.
 * После того, как строки перестанут считываться, выводиться общий результат по тексту с помощью того же метода printf();
 */

public class SecondMain {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(getFileOfPath()))) {

            String strLine; // Переменная которая хранит в себе строку
            int sumSpace = 0, sumSymbols = 0, sumWord = 0;

                while ((strLine = bufferedReader.readLine()) != null){
                    System.out.println("\n" + strLine);

                    sumSpace += countingOfSpace(strLine);
                    sumSymbols += countingOfSymbols(strLine);
                    sumWord += countingOfWord(strLine);

                }
            System.out.printf("\nОбщее кол-во пробелов: %d \nОбщее кол-во символов: %d \nОбщее кол-во символов без пробела: %d \nОбщее кол-во слов: %d", sumSpace, sumSymbols, sumSymbols - sumSpace, sumWord);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Метод позволяет ввести пользователю путь к файлу
     *
     * С помощью метода println() пользователю показывается пример того,как нужно вводить путь.
     * Благодаря scanner.nextLine() пользователь вводит путь.
     *
     * @return (String) путь в котором хранится файл
     */
    private static String getFileOfPath() {
        System.out.println("\n\tМногоуважаемый пользователь!\n" +
                "Введите путь до файла по примеру(снизу)\n" +
                "E:\\proekts\\TextVerification\\src\\ru\\aev\\text\\file.txt\n" +
                "Где E: - это диск на котором находится ваш файл, скорее всего он у вас другой,\n" +
                "Всё что идёт после : это папки в котором находится ваш файл который нужно проверить\n" +
                "Как не сложно догадаться после диска и каждой последующей папки нужно ставить \\ Примеры:\n" +
                "E:\\ \n" +
                "E:\\proekts\\TextVerification\n\n" +
                "Расширение файлу ставить обязательно!\n" +
                "Если по каким то причинам у вас не получилось, поздравляю вы д#$@`* ");

        return scanner.nextLine();
    }

    /**
     * Метод находит кол-во пробелов в предложен(ие/ях).
     *
     * Из предложен(ия/ий) с помощью метода replaceAll() удаляется всё кроме пробела,
     * с помощью метода length() узнается кол-во пробелов.
     *
     * @param str предложен(ие/ии) у котор(ого/ых) нужно узнать кол-во пробелов.
     * @return (int) кол-во пробелов в предложен(ие/ях).
     */
    private static int countingOfSpace(String str) {
        return str.replaceAll("[^ ]", "").length();
    }

    /**
     * Метод находит кол-во символов в предложен(ие/ях).
     *
     * С помощью метода length() узнается кол-во символов.
     *
     * @param str предложен(ие/ии) у котор(ого/ых) нужно узнать кол-во символов.
     * @return (int) кол-во символов в предложен(ие/ях).
     */
    private static int countingOfSymbols(String str) {
        return str.length();
    }

    /**
     * Метод находит кол-во слов в предложен(ие/ях).
     *
     * С помощью метода replaceAll() в предложен(ие/ях) сначало избавляется от знаков припенания и лишних знаков,
     * затем с помощью того же метода избавляется от лишних пробелов,
     * с помощью метода split() разбивает предложен(ие/ии) на отдельные слова.
     *
     * @param str предложен(ие/ии) у котор(ого/ых) нужно узнать кол-во слов
     * @return (int) кол-во слов в предложен(ие/ях).
     */
    private static int countingOfWord(String str) {
        str = str.replaceAll("[^-aA-zZаА-яЯ0-9 ]", " ");
        str = str.replaceAll("-", "");
        str = str.replaceAll(" {2,}", " ");
        return str.split(" ").length;
    }
}
